package pl.sda.wisielec;

import java.io.File;
import java.io.IOException;

public class Main {

	public static final String PATH_TO_WISIELEC_TXT = "C:\\Users\\RENT\\workspace6\\Wisielec\\wisielec\\wisielec.txt";

	public static void main(String[] args) throws IOException {
		Menu menu = new Menu();
		FileOperation fileOperation = new FileOperation();
		File file = new File(PATH_TO_WISIELEC_TXT);

		boolean flag = true;
		boolean flag2 = true;

		while (flag) {
			int opcja = menu.menu();
			switch (opcja) {
			case 1:
				menu.beforeTheStart();
				String wordFromFile = fileOperation.getWordFromFile(file);
				Word word = new Word(wordFromFile);
				System.out.println(word);
				while (flag2) {
					char character = word.typeChar();
					flag2 = word.gameVerification();
					word.checkWord(character);
					flag2 = word.gameVerification();
					System.out.println(word);
				}
				System.out.println("You win!");
				break;

			case 2:
				String wordToFile = menu.typeWord();
				fileOperation.saveWordInFile(file, wordToFile);
				break;
			case 3:
				System.out.println("End.");
				flag2 = false;
				break;
			}
		}
	}

}
