package pl.sda.wisielec;

import java.util.Scanner;

public class Menu {
	public int menu() {
		Scanner sc = new Scanner(System.in);

		System.out.println("1. Start.");
		System.out.println("2. Type word.");
		System.out.println("3. End.");
		int option = sc.nextInt();
		return option;
	}

	public void beforeTheStart() {
		Scanner sc = new Scanner(System.in);
		System.out.println("Game start.");
		sc.nextLine();
	}

	public String typeWord() {
		Scanner sc = new Scanner(System.in);

		System.out.println("Type the word to guess");
		String word = sc.nextLine();
		return word;
	}

}
