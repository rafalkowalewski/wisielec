package pl.sda.wisielec;

import java.util.Scanner;

public class Word {
	public char[] hiddenTab;
	public char[] shownTab;
	private int life = 5;

	public Word(String wordFromFile) {
		hiddenTab = wordFromFile.toCharArray();
		shownTab = new char[hiddenTab.length];
	}

	public void checkWord(char character) {
		int count = 0;

		for (int i = 0; i < shownTab.length; i++) {
			if (hiddenTab[i] == character) {
				shownTab[i] = character;
				count++;
			}
		}
		if (count == 0) {
			System.out.println("Wrong character. Try again.");
			life--;
		}
		System.out.println("Tou have " + life + " life.");
	}

	public boolean gameVerification() {
		if (hiddenTab.equals(shownTab)) {
			System.out.println(hiddenTab);
			System.out.println(shownTab);
			return false;
		}
		return true;
	}

	public char typeChar() {
		Scanner sc = new Scanner(System.in);
		System.out.println("Type the character.");
		String charString = sc.nextLine();
		char charChar = charString.charAt(0);
		return charChar;
	}

	@Override
	public String toString() {
		StringBuilder stringBuilder = new StringBuilder();
		for (int i = 0; i < shownTab.length; i++) {
			if (shownTab[i] == '\u0000') {
				stringBuilder.append(" _ ");
			} else {
				stringBuilder.append(shownTab[i]);
			}
		}
		return stringBuilder.toString();
	}

}
