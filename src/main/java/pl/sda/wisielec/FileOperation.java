package pl.sda.wisielec;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

public class FileOperation {

	public String getWordFromFile(File file) throws FileNotFoundException {
		Scanner sc = new Scanner(file);
		String word = null;
		while (sc.hasNextLine()) {
			word = sc.nextLine();
		}
		return word;
	}

	public void saveWordInFile(File file, String word) throws IOException {
		try (FileWriter fw = new FileWriter(file);
				BufferedWriter bw = new BufferedWriter(fw);
				PrintWriter out = new PrintWriter(bw)) {
			out.println(word);
		}
	}
}
